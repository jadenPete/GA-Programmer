package programmer

import "time"

type Options struct {
	/*
	 * PARAMETERS
	 */

	Input string
	Output string

	/*
	 * EXECUTION LIMITS
	 */

	Mem_size int
	Time_limit time.Duration

	/*
	 * POPULATION OPTIONS
	 */

	// The initial genome size
	Min_genome_size int // Inclusive
	Max_genome_size int // Exclusive

	Pop_size int
	World_size int

	// After this time, a world will be minimized to one population
	// Set to 0 for no minimization
	World_minimize time.Duration

	/*
	 * SELECTION AND BREEDING
	 */

	// How many theoretical mutations per member
	Mutation_rate float64

	// How many theoretical optimizations per the fittest members
	Optimization_rate float64

	// How many members to use during breeding
	Selection_size int

	// Whether the fittest members should be included in the next generation
	Selective_inclusion bool
}
