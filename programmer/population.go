package programmer

import (
	"genecode/optimizer"
	"math/rand"
	"sort"
	"strings"
	"sync"
)

type Population struct {
	members []Member
	generation int
	options *Options
}

var instructions = [...]string {
	">", "<", "+", "-", ",", ".", "[", "]",
}

func NewPopulation(options *Options) *Population {
	pop := Population {
		members: make([]Member, options.Pop_size),
		options: options,
	}

	for i := 0; i < options.Pop_size; i++ {
		genome := ""
		genome_size := randNumber(options.Min_genome_size, options.Max_genome_size)

		for j := 0; j < genome_size; j++ {
			genome += randInstruction()
		}

		pop.members[i] = *NewMember(genome, options)
	}

	return &pop
}

func (p *Population) GetMember(i int) *Member {
	return &p.members[i]
}

func (p *Population) GetGeneration() int {
	return p.generation
}

func (p *Population) Select() []int {
	var indexes = make([]int, p.options.Pop_size)
	var wg sync.WaitGroup

	wg.Add(p.options.Pop_size)

	for i := 0; i < p.options.Pop_size; i++ {
		go func(j int) {
			// Don't waste time on old members
			if !p.options.Selective_inclusion || j >= p.options.Selection_size || p.generation == 0 {
				p.members[j].Execute(p.members[j].Compile())
				p.members[j].Assess()
			}

			indexes[j] = j
			wg.Done()
		}(i)
	}

	wg.Wait()

	// Sort indexes by fitness
	// I was going to make shorter genome sizes a secondary
	// priority, but it has little effect and slows it down a lot
	sort.Slice(sort.IntSlice(indexes), func(i, j int) bool {
		return p.members[indexes[i]].GetFitness() > p.members[indexes[j]].GetFitness()
	})

	return indexes[:p.options.Selection_size]
}

func (p *Population) Breed(indexes []int) {
	// Backup the fittest members
	fittest := make([]Member, p.options.Selection_size)

	for i, index := range indexes {
		fittest[i] = p.members[index]
	}

	if p.options.Selective_inclusion {
		for i, member := range fittest {
			p.members[i] = member

			if rand.Float64() < p.options.Optimization_rate / float64(p.options.Selection_size) {
				p.members[i].genome = optimizer.Optimize(p.members[i].genome, p.options.Mem_size)
			}
		}
	}

	// Breed a new population
	var i int
	var wg sync.WaitGroup

	if p.options.Selective_inclusion {
		i = p.options.Selection_size
	}

	wg.Add(p.options.Pop_size - i)

	for ; i < p.options.Pop_size; i++ {
		go func(j int) {
			k := rand.Intn(p.options.Selection_size)
			l := rand.Intn(p.options.Selection_size)

			p.breedMembers(j, fittest[k], fittest[l])
			p.mutate(j)

			wg.Done()
		}(i)
	}

	wg.Wait()

	p.generation++
}

func (p *Population) breedMembers(i int, member1, member2 Member) {
	genome1 := member1.genome
	genome2 := member2.genome

	if len(genome1) > 0 {
		genome1 = genome1[:rand.Intn(len(genome1))]
	}

	if len(genome2) > 0 {
		genome2 = genome2[:rand.Intn(len(genome2))]
	}

	p.members[i] = *NewMember(genome1 + genome2, p.options)
}

func (p *Population) mutate(i int) {
	var genome = p.members[i].genome
	var buffer strings.Builder

	for _, gene := range genome {
		if rand.Float64() < p.options.Mutation_rate / float64(len(genome)) {
			switch rand.Intn(2) {
				// Modify
				case 0: buffer.WriteString(randInstruction())

				// Remove
				case 1:
			}
		} else {
			buffer.WriteString(string(gene))
		}
	}

	p.members[i].genome = buffer.String()
}

func randNumber(min, max int) int {
	diff := max - min

	if diff == 0 {
		return min
	}

	return rand.Intn(diff) + min
}

func randInstruction() string {
	return instructions[rand.Intn(8)]
}
