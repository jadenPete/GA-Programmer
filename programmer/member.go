package programmer

import (
	"genecode/executor"
	"strings"
	"time"
)

type Member struct {
	genome string
	options *Options

	output strings.Builder
	fitness float64
}

func NewMember(genome string, options *Options) *Member {
	return &Member {
		genome: genome,
		options: options,
	}
}

func (m *Member) GetGenome() string {
	return m.genome
}

func (m *Member) GetOutput() string {
	return m.output.String()
}

func (m *Member) GetFitness() float64 {
	return m.fitness
}

func (m *Member) Compile() []executor.Instruction {
	return executor.NewCompiler(m.genome).Compile()
}

func (m *Member) Execute(code []executor.Instruction) {
	// Buffer in case we want to stop execution
	c_done := make(chan bool, 1)
	c_quit := make(chan bool, 1)

	go executor.NewMachine(code, strings.NewReader(m.options.Input), &m.output, m.options.Mem_size).Execute(c_done, c_quit)

	select {
		case <-c_done:
			break

		case <-time.After(m.options.Time_limit):
			// Ask it to finish
			c_quit <- true

			// Wait for it to finish
			<-c_done

			// Discard any data in the output buffer
			// This might pose a problem if no output is avantageous
			m.output.Reset()
	}
}

func (m *Member) Assess() {
	real := m.output.String()
	expected := m.options.Output

	min_len := len(real)
	max_len := len(expected)

	if min_len > max_len {
		min_len = len(expected)
		max_len = len(real)
	}

	for i := 0; i < min_len; i++ {
		m.fitness += float64(255 - abs(expected[i] - real[i]))
	}

	m.fitness /= float64(max_len) * 255
}

func abs(x byte) byte {
	if x < 0 {
		return -x
	}

	return x
}
