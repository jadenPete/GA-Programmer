package programmer

import (
	"sync"
	"time"
)

type World struct {
	pops []Population
	creation time.Time
	options *Options
}

func NewWorld(options *Options) *World {
	world := World {
		pops: make([]Population, options.World_size),
		creation: time.Now(),
		options: options,
	}

	for i := 0; i < options.World_size; i++ {
		world.pops[i] = *NewPopulation(options)
	}

	return &world
}

func (w *World) GetPopulation(i int) *Population {
	return &w.pops[i]
}

func (w *World) GetCreation() time.Time {
	return w.creation
}

func (w *World) GetSize() int {
	return len(w.pops)
}

func (w *World) Select() [][]int {
	var indexes = make([][]int, len(w.pops))
	var wg sync.WaitGroup

	wg.Add(len(w.pops))

	for i := range w.pops {
		go func(j int) {
			indexes[j] = w.pops[j].Select()
			wg.Done()
		}(i)
	}

	wg.Wait()

	return indexes
}

func (w *World) Breed(indexes [][]int) {
	if time.Since(w.creation) > w.options.World_minimize {
		if !w.IsMinimized() {
			indexes = w.minimize(indexes)
		}

		w.pops[0].Breed(indexes[0])
	} else {
		var wg sync.WaitGroup
		wg.Add(len(w.pops))

		for i := range w.pops {
			go func(j int) {
				w.pops[j].Breed(indexes[j])
				wg.Done()
			}(i)
		}

		wg.Wait()
	}
}

func (w *World) IsMinimized() bool {
	return len(w.pops) == 1
}

func (w *World) minimize(indexes [][]int) [][]int {
	var fittest_p int
	var fittest_f float64

	for i := 1; i < len(indexes); i++ {
		fitness := w.pops[i].GetMember(indexes[i][0]).GetFitness()

		if fitness > fittest_f {
			fittest_p = i
			fittest_f = fitness
		}
	}

	w.pops = []Population {
		w.pops[fittest_p],
	}

	return [][]int {
		indexes[fittest_p],
	}
}
