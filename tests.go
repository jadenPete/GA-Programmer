package main

import (
	"fmt"
	"genecode/optimizer"
	"genecode/programmer"
	"time"
)

func production() {
	options := &programmer.Options {
		Input: "",
		Output: "hi",

		Mem_size: 1,
		Time_limit: 10 * time.Microsecond,

		Min_genome_size: 1,
		Max_genome_size: 10,
		Pop_size: 500,
		// World_size: 0,
		// World_minimize: 0,

		Mutation_rate: 2,
		Optimization_rate: 0,
		Selection_size: 20,
		Selective_inclusion: true,
	}

	pop := programmer.NewPopulation(options)

	for {
		indexes := pop.Select()
		fittest := pop.GetMember(indexes[0])

		fmt.Printf("%d\t%f\n", pop.GetGeneration(), fittest.GetFitness())

		if fittest.GetFitness() == 1 {
			fmt.Println()
			fmt.Printf("Normal: %s\n", fittest.GetGenome())
			fmt.Printf("Optimized: %s\n", optimizer.Optimize(fittest.GetGenome(), options.Mem_size))

			return
		}

		pop.Breed(indexes)
	}
}

func optimizerTest() {
	code := ""

	fmt.Printf("%s\n\n", code)
	fmt.Println(optimizer.Optimize(code, 1))
}
