package optimizer

import "strings"

func Optimize(code string, mem_size int) string {
	// The optimizations are sorted based on their
	// "dependencies", or which instructions they check
	// for; which are commented above each optimization

	// Cheap, independent optimizations are run earlier, because
	// they save a lot of resources for the more complicated ones

	// NOTE: A few optimizations require another character present,
	// but that's ok because they would have no effect therwise

	// '[', ']'
	code = jumps(code)

	// '.', ']'
	code = afterWrite(code)

	// '<', '>', '['
	code = illegalShifts(code, mem_size)

	// '<', '>', any
	code = extraShifts(code)

	// '+', '-', any
	code = mods(code)

	// ',', any
	code = reads(code)

	return code
}

func jumps(code string) string {
	var buffer strings.Builder
	var jumps = make([]int, 0)
	var offset = 0

	for i, inst := range code {
		switch inst {
			case '[':
				jumps = append(jumps, i - offset)
				buffer.WriteByte('[')

			case ']':
				if len(jumps) > 0 {
					jumps = jumps[:len(jumps) - 1]
					buffer.WriteByte(']')
				} else {
					offset++
				}

			default:
				buffer.WriteRune(inst)
		}
	}

	new_code := buffer.String()

	for i := len(jumps) - 1; i >= 0; i-- {
		new_code = new_code[:jumps[i]] + new_code[jumps[i] + 1:]
	}

	return new_code
}

func afterWrite(code string) string {
	for i := len(code) - 1; i >= 0; i-- {
		switch code[i] {
			case '.', ']':
				return code[:i + 1]
		}
	}

	return ""
}

func illegalShifts(code string, mem_size int) string {
	var current int
	var new_code strings.Builder

	for i, inst := range code {
		switch inst {
			case '>':
				if current < mem_size - 1 {
					new_code.WriteByte('>')
					current++
				}

			case '<':
				if current > 0 {
					new_code.WriteByte('<')
					current--
				}

			case '[':
				// Tracking the current cell after or during a loop is
				// difficult, unless there's only one byte of memory
				if mem_size == 1 {
					new_code.WriteByte('[')
				} else {
					return new_code.String() + code[i:]
				}

			default:
				new_code.WriteRune(inst)
		}
	}

	return new_code.String()
}

func extraShifts(code string) string {
	var current int
	var new_code strings.Builder

	for _, i := range code {
		switch i {
			case '>': current++
			case '<': current--

			default:
				if current < 0 {
					new_code.WriteString(strings.Repeat("<", -current))
				} else if current > 0 {
					new_code.WriteString(strings.Repeat(">", current))
				}

				current = 0
				new_code.WriteRune(i)
		}
	}

	return new_code.String()
}

func mods(code string) string {
	var new_code strings.Builder
	var value int

	for _, i := range code {
		switch i {
			case '+': value++
			case '-': value--

			default:
				if value < 0 {
					new_code.WriteString(strings.Repeat("-", -value))
				} else {
					new_code.WriteString(strings.Repeat("+", value))
				}

				new_code.WriteRune(i)
				value = 0
		}
	}

	return new_code.String()
}

func reads(code string) string {
	var reading bool
	var new_code strings.Builder

	for _, i := range code {
		if i == ',' {
			reading = true
		} else {
			if reading {
				new_code.WriteByte(',')
				reading = false
			}

			new_code.WriteRune(i)
		}
	}

	return new_code.String()
}
