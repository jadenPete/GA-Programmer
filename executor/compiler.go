package executor

type Compiler struct {
	input string
	input_pos int

	output []Instruction
}

func NewCompiler(code string) *Compiler {
	return &Compiler {
		input: code,
	}
}

func (c *Compiler) Compile() []Instruction {
	c.mapOccurences()
	c.mapJumps()

	return c.output
}

func (c *Compiler) mapOccurences() {
	for ; c.input_pos < len(c.input); c.input_pos++ {
		switch c.input[c.input_pos] {
			case '>', '<', '+', '-', ',', '.':
				c.compileInstruction()

			case '[', ']':
				c.compileJumpInstruction()
		}
	}
}

func (c *Compiler) mapJumps() {
	jumps := make([]int, 0)

	for i := range c.output {
		switch c.output[i].char {
			case '[':
				jumps = append(jumps, i)

			case ']':
				if len(jumps) > 0 {
					depth := len(jumps) - 1
					last := jumps[depth]

					c.output[last].arg = i
					c.output[i].arg = last

					jumps = jumps[:depth]
				} else {
					c.output[i].arg = -1
				}
		}
	}

	for _, i := range jumps {
		c.output[i].arg = -1
	}
}

func (c *Compiler) compileJumpInstruction() {
	c.output = append(c.output, Instruction {
		char: c.input[c.input_pos],
	})
}

func (c *Compiler) compileInstruction() {
	inst := Instruction {
		char: c.input[c.input_pos],
		arg: 1,
	}

	// Count number of occurences
	for c.input_pos < len(c.input) - 1 && c.input[c.input_pos + 1] == inst.char {
		inst.arg++
		c.input_pos++
	}

	c.output = append(c.output, inst)
}
