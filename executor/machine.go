package executor

import "io"

type Machine struct {
	code []Instruction
	code_pos int

	mem []byte
	mem_pos int

	input io.Reader
	output io.Writer
}

type Instruction struct {
	char byte
	arg int
}

func NewMachine(code []Instruction, input io.Reader, output io.Writer, mem_size int) *Machine {
	return &Machine {
		code: code,
		mem: make([]byte, mem_size),

		input: input,
		output: output,
	}
}

func (m *Machine) Execute(c_done chan<- bool, c_quit <-chan bool) {
	defer func() {
		c_done <- true
	}()

	for ; m.code_pos < len(m.code); m.code_pos++ {
		select {
			case <-c_quit:
				return

			default:
				arg := m.code[m.code_pos].arg

				switch m.code[m.code_pos].char {
					case '>': m.nextCell(arg)
					case '<': m.previousCell(arg)

					case '+': m.mem[m.mem_pos] += byte(arg)
					case '-': m.mem[m.mem_pos] -= byte(arg)

					case ',': m.readChars()
					case '.': m.writeChars()

					case '[': m.forwardJump()
					case ']': m.backwardJump()
				}
		}
	}
}

func (m *Machine) nextCell(count int) {
	m.mem_pos += count

	if m.mem_pos >= len(m.mem) {
		m.mem_pos = len(m.mem) - 1
	}
}

func (m *Machine) previousCell(count int) {
	m.mem_pos -= count

	if m.mem_pos < 0 {
		m.mem_pos = 0
	}
}

func (m *Machine) readChars() {
	buffer := make([]byte, 1)
	_, err := m.input.Read(buffer)

	if err == io.EOF {
		m.mem[m.mem_pos] = 0
	} else if err == nil {
		m.mem[m.mem_pos] = buffer[0]
	} else {
		panic(err)
	}
}

func (m *Machine) writeChars() {
	buffer := []byte { m.mem[m.mem_pos] }

	for i := 0; i < m.code[m.code_pos].arg; i++ {
		n, err := m.output.Write(buffer)

		if n != 1 {
			panic("Error writing bytes")
		}

		if err != nil {
			panic(err)
		}
	}
}

func (m *Machine) forwardJump() {
	if m.mem[m.mem_pos] == 0 {
		m.jump()
	}
}

func (m *Machine) backwardJump() {
	if m.mem[m.mem_pos] != 0 {
		m.jump()
	}
}

func (m *Machine) jump() {
	target := m.code[m.code_pos].arg

	if target != -1 {
		m.code_pos = target
	}
}
